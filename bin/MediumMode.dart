import 'Question.dart';

class Medium extends Question {
  String Hint1 = '';
  String Hint2 = '';
  String Hint3 = '';
  Medium(String Hint1, String Hint2, String Hint3) {
    this.Hint1 = Hint1;
    this.Hint2 = Hint2;
    this.Hint3 = Hint3;
  }
  String show() {
    return 'Hint 1 : $Hint1 \nHint 2 : $Hint2  \nHint 3 : $Hint3';
  }
}
