class Score {
  int score = 0;

  Score(int score) {
    this.score = score;
  }

  void showscore(int score) {
    if (score == 0) {
      print('Your got score $score/5.Try again 🌑🌑🌑🌑🌑');
    } else if (score == 1) {
      print('Your got score $score/5.Bad 🌕🌑🌑🌑🌑');
    } else if (score == 2) {
      print('Your got score $score/5.Good 🌕🌕🌑🌑🌑');
    } else if (score == 3) {
      print('Your got score $score/5.Good Job 🌕🌕🌕🌑🌑');
    } else if (score == 4) {
      print('Your got score $score/5.Very Good 🌕🌕🌕🌕🌑');
    } else if (score == 5) {
      print('Your got score $score/5.Prefect 💯 🌕🌕🌕🌕🌕');
    }
  }
}
