import 'package:dart_application_1/dart_application_1.dart' as dart_application_1;
import 'dart:ffi';
import 'dart:io';

import 'EasyMode.dart';
import 'HardMode.dart';
import 'MediumMode.dart';
import 'Score.dart';

void main(List<String> arguments) {
  print("\x1B[2J\x1B[0;0H");
  print('♫╦╦╦═╦╗╔═╦═╦♫═╦═╗ ♫╗');
  print('║║║║╩╣╚╣═╣║║║║║╩╣ ║╚╗╔═╗');
  print('╚══╩♫╩═╩═╩═╩╩╩╩═╝ ║╔╣║║║🌈');
  print('Ƹ̵̡Ӝ̵̨̄Ʒ       .•*     ╚═╝╚♫╝  ╔♫╦═╗╔══╦═╗');
  print('•.¸¸.•*´¨`*♥ •.¸¸.•*´¨`*  ║╬║╬╚╣║║║╩╣');
  print('                          ╠╗╠══╩╩╩╩♫╝');
  print('             •*´¨`•.¸.•Ƹ̵̡Ӝ̵̨̄Ʒ╚═♫ ');
  print('    ♥ •.¸¸.•*╔♫');
  print('             ║║ 🌈');
  print('╔♫═╦═╗╔╦╗╔╦══╣║─╔══╦╗╔╦══♫══╦══╦╦♫╗╔══╗');
  print('║╔╗║╔╗╬╣╚╝║╔╗║║─║╔╗║║║║║═╣══╣══╬╣╔╗╣╔╗║');
  print('║╔╗║║║║║║║║╔╗║╚╗║╚╝║╚╝║║═╬══╠══║║║║║╚╝║');
  print('╚╝╚╩╝╚╩╩╩╩♫╝╚╩═╝╚═╗╠══╩══╩═♫╩══╩╩╝╚╩═╗║');
  print('                ╔═╝║               ╔═╝║ 🌈');
  print('                ╚══♫  •*´¨`•.¸.•Ƹ̵̡Ӝ̵̨̄Ʒ╚♫═╝');
  print('');
  print('    ∵*.•´¸.•*´✶´♡');
  print('  ° ☆ ° ˛*˛☆_Π______˚☆');
  print('*˚ ˛★˛•*/________/ ~ ⧹。˚ ˚');
  print(' ˚ ˛•˛•˚｜ 田田  ｜ 門｜ ˚');
  print('     🌷╬╬🌷╬╬🌷╬╬🌷╬╬🌷');
  print('');
  print('─────────────────────────────────────────────');
  print('╔╦♫╦╦╗   ╔♫                      ╔══╗');
  print('║║║║║╚╦═╗║╚╗╔╦╦═♫╦╦╦╗╔═♫╦═╗╔═♫╦═╗╚═╗║');
  print('║║║║║║║╬╚╣╔╣║║║╬║║║╔╝║║║║╬╚╣║║║╩╣ ╔╔╝');
  print('♫═╩═╩╩╩═♫╩═╝╠╗╠═╩═╩╝ ╚╩═╩═♫╩╩╩╩═♫ ╠╣•.¸.•Ƹ̵̡Ӝ̵̨̄Ʒ 🌈');
  print('            ♫═╝                   ╚♫');
  print('─────────────────────────────────────────────');
  print('');

  String answer;
  String again;
  String namePlayer;
  String choice;
  int score = 0;
  print('             Write your name Player          ');
  namePlayer = stdin.readLineSync()!;
  print('══════════════════════════════════════════════✏️');
  print('');
  print('──────────────────────────────────────────────');
  print('👩‍💻 Player : $namePlayer                          ');
  print('══════════════════════════════════════════════');
  sleep(Duration(seconds: 5));
  print("\x1B[2J\x1B[0;0H");
    print('      ♫═╦═╗  ╔♫   ╔═╦═♫');
  print('      ║║║║╠═╗╠╬═╦╗║║║║╠═╦═╦╦╦╗');
  print('      ║║║║║╬╚╣║║║║║║║║║╩╣║║║║♫');
  print('      ╚╩♫╩╩══╩╩╩═╝╚╩═╩╩═╩╩♫╩═╝');
  print("Choose your level do you want to play!<3");
  print('  ╔═══════════════════════════════╗');
  print(
      '  ║           🤪 Easy  ✨         ║ 📑 Suitable for children under the age of 7-11 👧 🧒');
  print(
      '  ║          😃 Medium ✨         ║ 📑 Suitable for children under the age of 12-16 👦 👩');
  print(
      '  ║           😈 Hard  ✨         ║ 📑 Suitable for children over the age of 17 and must have sufficient knowledge about kind of breed🧑 👩');
  print('  ║                          Exit ║ ');
  print('  ╚═══════════════════════════════╝');
choice = stdin.readLineSync()!;
  print('══════════════════════════════════════════════✏️');
  print('');
  while (true) {
    Score collecScore = Score(score);
    if (choice == 'Easy') {
      sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      var easy = Easy('I am small.', 'I like carrot.', 'I like to jump.');
      print('');
      print(easy.show());
      print('');
      print('Number 1 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Ant  2. Rabbit  3. Wolf');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      print('');
      if (answer == 'Rabbit') {
        score = score + 1;
        print('Right ⭕️');
        print('             ,\  ');
        print("             \\\  ");
        print("              \` ,\  ");
        print('         __,.-" =__)  ');
        print('       ."        )  ');
        print('    ,_/   ,    \/\  ');
        print('    \_|    )_-\ \_-` ');
        print('       `-----` `--` ');
      } else {
        print('Wrong ❌');
      }
      print('─────────────────────────────────────────────');
      print('');
      sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      easy = Easy('I like to say meow.', 'I like fish', 'I can climb tree.');
      print(easy.show());
      print('');
      print('Number 2 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Frog  2. Cat  3. Monkey');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Cat') {
        score = score + 1;
        print('Right ⭕️');
        print('                   _ |\_ ');
        print("                   \` ..\  ");
        print('              __,.-" =__Y=  ');
        print('           ."        ) ');
        print('      _    /   ,    \/\  ');
        print('     ((____|    )_-\ \_-` ');
        print("      `-----'`-----` `--` ");
      } else {
        print('Wrong ❌');
      }
      print('─────────────────────────────────────────────');
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      easy = Easy('I am insect.', 'I have eight legs.', 'I can weave.');
      print(easy.show());
      print('');
      print('Number 3 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Spider 2. Ant  3. Grasshopper');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Spider') {
        score = score + 1;
        print('Right ⭕️');
        print('       ,,, ');
        print("       \\\  ");
        print(' .---.  /// ');
        print('(:::::)(_)(): ');
        print(" `---'  \\\  ");
        print('        /// ');
        print("       '''  ");
      } else {
        print('Wrong ❌');
      }
      print('─────────────────────────────────────────────');
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      easy = Easy('I have two leg.', 'I have two wings.', 'I can fly.');
      print(easy.show());
      print('');
      print('Number 4 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Snake 2. Lion  3. Bird');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Bird') {
        score = score + 1;
        print('Right ⭕️');
        print('              __ ');
        print("             /'{> ");
        print('         ____) (____ ');
        print("       //'--;   ;--'\\  ");
        print("      ///////\_/\\\\\\\  ");
        print('             m m ');
      } else {
        print('Wrong ❌');
      }
      print('─────────────────────────────────────────────');
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      easy = Easy('I have four legs.', 'I can bark.', 'I cant fly.');
      print(easy.show());
      print('');
      print('Number 5 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Bird 2. Cat 3. Dog');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Dog') {
        score = score + 1;
        print('Right ⭕️');
        print('           __ ');
        print("      (___()'`; ");
        print('      /,    /` ');
        print('      \\"--\\ ');
      } else {
        print('Wrong ❌');
      }
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      collecScore.showscore(score);
    } else if (choice == 'Medium') {
      score = 0;
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      var medium = Medium(
          'In the morning, the alarm will sound.',
          'Have two wings. Two feet.',
          'I can lay eggs and I like to say buk buk. ');
      print('');
      print(medium.show());
      print('');
      print('Number 1 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Chicken 2. Duck 3. Owl');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Chicken') {
        score = score + 1;
        print('Right ⭕️');
        print('      ,~. ');
        print("   ,-'__ `-, ");
        print("  {,-'  `. }              ,') ");
        print(" ,( a )   `-.__         ,',')~, ");
        print("<=.) (         `-.__,==' ' ' '}");
        print("  (   )                      /");
        print("   `-'\   ,                  )");
        print('       |  \        `~.      /');
        print('       \   `._        \    /');
        print("        \     `._____,'   / ");
        print("         `-.            ,'");
        print("            `-.      ,-'");
        print("               `~~~~'");
        print("               //_||");
        print("            __//--'/`");
        print("          ,--'/`  '");
        print("             '");
      } else {
        print('Wrong ❌');
      }
      print('─────────────────────────────────────────────');
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      medium = Medium('Im fast and I have four legs.',
          'I have sharp fangs and claws.', 'I am big and I can roar');
      print('');
      print(medium.show());
      print('');
      print('Number 2 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Lion 2. Whale 3. Elephant');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Lion') {
        score = score + 1;
        print('Right ⭕️');
        print('      /";;:;;"\ ');
        print("    (:;/\,-,/\;;)");
        print("   (:;{  d b  }:;)");
        print("    (:;\__Y__/;;)-----------,,_");
        print("     ,..\  ,..\      ___/___)__`\ ");
        print("    (,,,)~(,,,)`-._##____________) ");
      } else {
        print('Wrong ❌');
      }
      print('─────────────────────────────────────────────');
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      medium = Medium('I can swim in water and I can walk on the ground.',
          'I am big and I have a strong jaw.', 'I am reptile and I eat meat');
      print('');
      print(medium.show());
      print('');
      print('Number 3 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Crocodile 2. Salamander 3. Shark');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Crocodile') {
        score = score + 1;
        print('Right ⭕️');
        print('            .-._   _ _ _ _ _ _ _ _ ');
        print(" .-''-.__.-'00  '-' ' ' ' ' ' ' ' '-. ");
        print("'.___ '    .   .--_'-' '-' '-' _'-' '. ");
        print("  V: V 'vv-'   '_   '.       .'  _..' '.'. ");
        print("   '=.____.=_.--'   :_.__.__:_   '.   : : ");
        print("           (((____.-'        '-.  /   : : ");
        print("                             (((-'\ .' /  ");
        print("                           _____..'  .' ");
        print("                          '-._____.-' ");
      } else {
        print('Wrong ❌');
      }
      print('─────────────────────────────────────────────');
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      medium = Medium(
          'I look like dog but I big more than dog.',
          'I am fast and I sharp fangs and claws.',
          'I live in a herd and I live in forest');
      print('');
      print(medium.show());
      print('');
      print('Number 4 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Dog 2. Wolf 3. Hyena');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Wolf') {
        score = score + 1;
        print('Right ⭕️');
        print('                        ,     , ');
        print("                        |\---/|  ");
        print("                       /  , , |  ");
        print("                  __.-'|  / \ /  ");
        print("         __ ___.-'        ._O|  ");
        print("      .-'  '        :      _/  ");
        print("     / ,    .        .     |  ");
        print("    :  ;    :        :   _/ ");
        print("    |  |   .'     __:   / ");
        print("    |  :   /'----'| \  | ");
        print("    \  |\  |      | /| | ");
        print("     '.'| /       || \ | ");
        print("     | /|.'       '.l \\_ ");
        print("     || ||             '-' ");
        print("     '-''-' ");
      } else {
        print('Wrong ❌');
      }
      print('─────────────────────────────────────────────');
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      medium = Medium('I can live in water and I can live on ground.',
          'I eat insect and I no walk but I jump.', 'I live in pond');
      print('');
      print(medium.show());
      print('');
      print('Number 5 : Who am I ❓');
      print('');
      print('Choose your answer : 1. Frog 2. Salamader 3. Dolphin');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Frog') {
        score = score + 1;
        print('Right ⭕️');
        print('        ()-()  ');
        print("      .-(___)-.  ");
        print("       _<   >_  ");
        print("       \/   \/  ");
      } else {
        print('Wrong ❌');
      }
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      collecScore.showscore(score);
    } else if (choice == 'Hard') {
      score = 0;
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      var hard = Hard(
          'The highlight is that the edges of both ears have white borders, which is the origin of the name.',
          'They live in small flocks in large trees or fruit trees. Eat foods such as fruits such as mangoes and nectar from flowers.',
          'I am small bat.');
      print('');
      print(hard.show());
      print('');
      print('Number 1 : What kind of breed am I ❓');
      print('');
      print(
          'Choose your answer : 1. Lesser Short-nosed Fruit Bat 2. Northern tailless fruit bat 3. Little tube-nosed bat');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Lesser Short-nosed Fruit Bat') {
        score = score + 1;
        print('Right ⭕️');
        print('    =/\                 /\=  ');
        print("    / \'._   (\_/)   _.'/ \   ");
        print("   / .''._'--(o.o)--'_.''. \   ");
        print("  /.' _/ |`'=/   \='`| \_ `.\   ");
        print(" /` .' `\;-,'\___/',-;/` '. '\   ");
        print("/.-'       `\(-V-)/`       `-.\   ");
        print("`            " "            ` ");
      } else {
        print('Wrong ❌');
      }
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      hard = Hard(
          'There are usually three colors of hair on the body: white, black and brown.',
          'The forehead is clearly set, the ears are long, the fur is short, straight, the tail is moderately long',
          'I am dog.');
      print('');
      print(hard.show());
      print('');
      print('Number 2 : What kind of breed am I ❓');
      print('');
      print('Choose your answer : 1. Beagle 2. Bulldog 3. Pomeranian');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Beagle') {
        score = score + 1;
        print('Right ⭕️');
        print('       _=,_');
        print("    o_/6 /#\ ");
        print("    \__ |##/ ");
        print("     ='|--\ ");
        print("       /   #'-.   ");
        print("       \#|_   _'-. / ");
        print('        |/ \_( # |" ');
        print('       C/ ,--___/ ');
      } else {
        print('Wrong ❌');
      }
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      hard = Hard(
          'The nose is upturned, the upper carapace is smooth. brown or gray There is a long black line 3 lines.',
          'Brown headed female The males in the breeding season are gray or white.The forehead has orange or red streaks',
          'The forelegs have 5 claws, the upper carapace is 60 cm long.I am turtle');
      print('');
      print(hard.show());
      print('');
      print('Number 3 : What kind of breed am I ❓');
      print('');
      print(
          'Choose your answer : 1. Heosemys grandis 2. Cyclemys dentata 3. Callagur borneoensis');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Callagur borneoensis') {
        score = score + 1;
        print('Right ⭕️');
        print('                    __    ');
        print("         .,-;-;-,. /'_\     ");
        print("      _/_/_/_|_\_\) /     ");
        print("     '-<_><_><_><_>=/\     ");
        print("       `/_/====/_/-'\_\    ");
      } else {
        print('Wrong ❌');
      }
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      hard = Hard(
          'There are 9 dark brown points on the body, including at the tip of the four feet.',
          'The tip of both ears, the tip of the tail, on the nose and on the genitals.',
          'I am cat.');
      print('');
      print(hard.show());
      print('');
      print('Number 4 : What kind of breed am I ❓');
      print('');
      print(
          'Choose your answer : 1. Siamese 2. Scottish Fold 3. American Shorthair');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Siamese') {
        score = score + 1;
        print('Right ⭕️');
        print('       _');
        print("       \`*-. ");
        print("        )  _`-. ");
        print("       .  : `. .     ");
        print("       : _   '  \   ");
        print("       ; *` _.   `*-._      ");
        print("       `-.-'          `-.   ");
        print('         ;       `       `.   ');
        print('         :.       .        \    ');
        print("         :.       .        \    ");
        print("         '  `+.;  ;  '      :   ");
        print("         :  '  |    ;       ;-. ");
        print("         ; '   : :`-:     _.`* ; ");
        print("      .*' /  .*' ; .*`- +'  `*'  ");
        print("      `*-*   `*-*  `*-*' ");
      } else {
        print('Wrong ❌');
      }
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      hard = Hard(
          'Scientific name: Psittacula Krameri, cultured new colors without ending.',
          'physique, shape, color of feathers and a tail that is longer than the body.',
          'I am parrot.');
      print('');
      print(hard.show());
      print('');
      print('Number 5 : What kind of breed am I ❓');
      print('');
      print(
          'Choose your answer : 1. Eclectus 2. Ringnecked Parakeet 3. Conure');
      answer = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      if (answer == 'Ringnecked Parakeet') {
        score = score + 1;
        print('Right ⭕️');
        print('                           .');
        print("                          | \/| ");
        print("  (\   _                  ) )|/|  ");
        print("      (/            _----. /.'.' ");
        print(".-._________..      .' @ _\  .'   ");
        print("'.._______.   '.   /    (_| .') ");
        print("  '._____.  /   '-/      | _.'  ");
        print("   '.______ (         ) ) \  ");
        print("     '..____ '._       )  )  ");
        print("        .' __.--\  , ,  // (( ");
        print("        '.'     |  \/   (_.'( ");
        print("                '   \ .' ");
        print("                 \   ( ");
        print("                  \   '. ");
        print("                   \   '. ");
        print("                    '-'-'  ");
      } else {
        print('Wrong ❌');
      }
      print('');
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      collecScore.showscore(score);
    } else if (choice == 'Exit') {
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      print('Bye bye $namePlayer.See you again.😭');
      print("╔══♫             ╔♫");
      print("║╔╗║             ║║");
      print("║╚╝╚╦╗ ╔╦══♫     ♫╚═╦╗ ♫╦══╗");
      print("║╔═╗║║ ║║║═╣     ║╔╗║║ ║║║═╣");
      print("║╚═╝║╚♫╝║║═╣     ║╚╝║╚═╝║║═╣");
      print("╚♫══╩═╗╔╩══♫     ╚♫═╩═╗╔╩══♫ •*´¨`•.¸.•Ƹ̵̡Ӝ̵̨̄Ʒ 🌈");
      print("    ╔═╝║            ╔═╝║");
      print("    ♫══╝            ╚══♫");
      print('______ (¯`v´¯)');
      print('______(¯`(█)´¯)__(¯`v´¯)');
      print('_______(_.^._)__(¯`(█)´¯)');
      print('_________(¯`v´¯)´(_.^._)');
      print('________(¯`(█)´¯)(¯`v´¯)');
      print('_________(_.^._)(¯`(█)´¯)');
      print('____(¯`v´¯)_____ (_.^._)(¯`v´¯)');
      print('___ (¯`(█)´¯)_ (¯`v´¯)___(¯`(█)´¯)');
      print('____ (_.^._)_(¯`(█)´¯)___(_.^._)');
      print('____ (¯`v´¯)´ (_.^._) (¯`v´¯)');
      print('____(¯`(█)´¯)_█___(¯`(█)´¯)');
      print('_____(_.^._)_█____(_.^._)');
      print('___________█');
      print('_________█');
      print('_______█___██_.██');
      print('______█___████████');
      print('______█___████████');
      print('_______█___ █████');
      print('________█____███');
      print('_________█____█');
      print('__________█______███__███');
      print('_██_.██___█_____█████████');
      print('_██_.██___█_____█████████');
      print('███████___█____█████████');
      print('███████___█_____ ███████');
      print('_ █████____█_______█████');
      print('___███_____█_________█');
      print('____█______█');
      break;
    }
    print("Back to Menu or Exit ❓ ( Menu / Exit )");
    String choice1 = stdin.readLineSync()!;
    print('══════════════════════════════════════════════✏️');
    if (choice1 == 'Menu') {
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      score = 0;
      print('──────────────────────────────────────────────');
      print('👩‍💻 Player : $namePlayer                          ');
      print('══════════════════════════════════════════════');
      print('      ♫═╦═╗  ╔♫   ╔═╦═♫');
      print('      ║║║║╠═╗╠╬═╦╗║║║║╠═╦═╦╦╦╗');
      print('      ║║║║║╬╚╣║║║║║║║║║╩╣║║║║♫');
      print('      ╚╩♫╩╩══╩╩╩═╝╚╩═╩╩═╩╩♫╩═╝');
      print("Choose your level do you want to play!<3");
      print('  ╔═══════════════════════════════╗');
      print(
          '  ║           🤪 Easy  ✨         ║ 📑 Suitable for children under the age of 8-11 👧 🧒');
      print(
          '  ║          😃 Medium ✨         ║ 📑 Suitable for children under the age of 12-16 👦 👩');
      print(
          '  ║           😈 Hard  ✨         ║ 📑 Suitable for children over the age of 17 and must have sufficient knowledge about kind of breed🧑 👩');
      print('  ║                          Exit ║ ');
      print('  ╚═══════════════════════════════╝');
      choice = stdin.readLineSync()!;
      print('══════════════════════════════════════════════✏️');
      print('');
    } else if (choice1 == 'Exit') {
            sleep(Duration(seconds: 3));
      print("\x1B[2J\x1B[0;0H");
      print('Bye bye $namePlayer.See you again.😭');
      print("╔══♫             ╔♫");
      print("║╔╗║             ║║");
      print("║╚╝╚╦╗ ╔╦══♫     ♫╚═╦╗ ♫╦══╗");
      print("║╔═╗║║ ║║║═╣     ║╔╗║║ ║║║═╣");
      print("║╚═╝║╚♫╝║║═╣     ║╚╝║╚═╝║║═╣");
      print("╚♫══╩═╗╔╩══♫     ╚♫═╩═╗╔╩══♫ •*´¨`•.¸.•Ƹ̵̡Ӝ̵̨̄Ʒ 🌈");
      print("    ╔═╝║            ╔═╝║");
      print("    ♫══╝            ╚══♫");
      print('______ (¯`v´¯)');
      print('______(¯`(█)´¯)__(¯`v´¯)');
      print('_______(_.^._)__(¯`(█)´¯)');
      print('_________(¯`v´¯)´(_.^._)');
      print('________(¯`(█)´¯)(¯`v´¯)');
      print('_________(_.^._)(¯`(█)´¯)');
      print('____(¯`v´¯)_____ (_.^._)(¯`v´¯)');
      print('___ (¯`(█)´¯)_ (¯`v´¯)___(¯`(█)´¯)');
      print('____ (_.^._)_(¯`(█)´¯)___(_.^._)');
      print('____ (¯`v´¯)´ (_.^._) (¯`v´¯)');
      print('____(¯`(█)´¯)_█___(¯`(█)´¯)');
      print('_____(_.^._)_█____(_.^._)');
      print('___________█');
      print('_________█');
      print('_______█___██_.██');
      print('______█___████████');
      print('______█___████████');
      print('_______█___ █████');
      print('________█____███');
      print('_________█____█');
      print('__________█______███__███');
      print('_██_.██___█_____█████████');
      print('_██_.██___█_____█████████');
      print('███████___█____█████████');
      print('███████___█_____ ███████');
      print('_ █████____█_______█████');
      print('___███_____█_________█');
      print('____█______█');
      break;
    }
  }
}