import 'Question.dart';

class Hard extends Question {
  String Hint1 = '';
  String Hint2 = '';
  String Hint3 = '';
  Hard(String Hint1, String Hint2, String Hint3) {
    this.Hint1 = Hint1;
    this.Hint2 = Hint2;
    this.Hint3 = Hint3;
  }
  String show() {
    return 'Sentence 1 : $Hint1 \nSentence 2 : $Hint2  \nSentence 3 : $Hint3';
  }
}