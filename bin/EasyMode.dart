import 'Question.dart';

class Easy extends Question {
  String Hint1 = '';
  String Hint2 = '';
  String Hint3 = '';
  Easy(String Hint1, String Hint2, String Hint3) {
    this.Hint1 = Hint1;
    this.Hint2 = Hint2;
    this.Hint3 = Hint3;
  }
  String show() {
    return 'Nature 1 : $Hint1 \nNature 2 : $Hint2  \nNature 3 : $Hint3';
  }
}